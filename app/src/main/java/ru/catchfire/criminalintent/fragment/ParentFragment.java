package ru.catchfire.criminalintent.fragment;

import android.support.v4.app.Fragment;
import android.widget.Toast;

/**
 * Parent фрагментов
 */

public class ParentFragment extends Fragment {

    public void showToast(String message){
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }


}

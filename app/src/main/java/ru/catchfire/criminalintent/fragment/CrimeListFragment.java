package ru.catchfire.criminalintent.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ru.catchfire.criminalintent.R;
import ru.catchfire.criminalintent.activity.CrimePagerActivity;
import ru.catchfire.criminalintent.model.Crime;
import ru.catchfire.criminalintent.model.CrimeLab;
import ru.catchfire.criminalintent.utils.ConstantManager;

/**
 * Выводит на экран список всех преступлений
 * CrimeAdapter реализует паттерн переиспользование View для вывода списка (аналогия с RecyclerView)
 */

public class CrimeListFragment extends ListFragment {

    private ArrayList<Crime> mCrimes;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //явно указывает FM, что фрагмент получит вызов метода создание меню
        setHasOptionsMenu(true);
        getActivity().setTitle(getString(R.string.crimes_title));

        mCrimes = CrimeLab.get(getActivity()).getCrimes();

        CrimeAdapter adapter = new CrimeAdapter(mCrimes);
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Crime c = ((CrimeAdapter)getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(), CrimePagerActivity.class);
        i.putExtra(ConstantManager.CRIME_UUID, c.getUUID());
        startActivity(i);
    }

    public void showToast(String message){
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    private class CrimeAdapter extends ArrayAdapter<Crime> {

        public CrimeAdapter(ArrayList<Crime> crimes) {
            super(getActivity(), 0, crimes);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            //для получения представления заполняем его
            if (convertView == null){
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_crime, null);
            }

            Crime crime = getItem(position);

            TextView title = (TextView) convertView.findViewById(R.id.crime_list_title_textview);
            title.setText(crime.getTitle());

            TextView date = (TextView) convertView.findViewById(R.id.crime_list_date_textview);
            date.setText(crime.getDate().toString());

            CheckBox isSolved = (CheckBox) convertView.findViewById(R.id.crime_list_solved_checkbox);
            isSolved.setChecked(crime.isSolved());

            return convertView;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //перезагрузка списка
        ((CrimeAdapter)getListAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_crime_list, menu);
    }
}

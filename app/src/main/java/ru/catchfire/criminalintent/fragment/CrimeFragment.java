package ru.catchfire.criminalintent.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.UUID;

import ru.catchfire.criminalintent.R;
import ru.catchfire.criminalintent.model.Crime;
import ru.catchfire.criminalintent.model.CrimeLab;
import ru.catchfire.criminalintent.utils.ConstantManager;

/**
 * Служит для отображения одного объекта Crime
 * Первый способ (1) отображение выбранного Crime из CrimeListFragment в CrimeFragment
 * Второй способ (2) через задания аргумента
 */

public class CrimeFragment extends ParentFragment {

    private Crime mCrime;
    private Button mDateButton;

//(2)
    public static CrimeFragment newInstance(UUID uuid){
        Bundle args = new Bundle();
        args.putSerializable(ConstantManager.CRIME_UUID, uuid);
        CrimeFragment fragment = new CrimeFragment();
        fragment.setArguments(args);
        return fragment;
    }
//(2)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID uuid = (UUID) getArguments().getSerializable(ConstantManager.CRIME_UUID);
        mCrime = CrimeLab.get(getContext()).getCrime(uuid);

//         (1)
//         UUID uuid = (UUID) getActivity().getIntent().getSerializableExtra(ConstantManager.CRIME_UUID);
//         mCrime = CrimeLab.get(getActivity()).getCrime(uuid);
//         (1)
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_crime, container, false);

        EditText titleField = (EditText) view.findViewById(R.id.crime_title);
        titleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mCrime.setTitle(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mDateButton = (Button) view.findViewById(R.id.crime_date);
        updateDate();
        mDateButton.setEnabled(true);
        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                DatePickerFragment dialog = DatePickerFragment.newInstance(mCrime.getDate());
                //Toast.makeText(getActivity(), mCrime.getDate().toString(), Toast.LENGTH_SHORT).show();
                dialog.setTargetFragment(CrimeFragment.this, ConstantManager.REQUEST_DATE);
                dialog.show(manager, ConstantManager.DATE_PICKER_FRAGMENT);
            }
        });

        CheckBox solvedCheckBox = (CheckBox) view.findViewById(R.id.crime_solved);
        solvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCrime.setSolved(isChecked);
            }
        });

//(1)(2)
         titleField.setText(mCrime.getTitle());
         solvedCheckBox.setChecked(mCrime.isSolved());
//(1)(2)

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        if (requestCode == ConstantManager.REQUEST_DATE){
            Date date = (Date) data.getSerializableExtra(ConstantManager.DATE_PICKER_FRAGMENT);
            mCrime.setDate(date);
            updateDate();
        }
    }

    private void updateDate() {
        mDateButton.setText(mCrime.getDate().toString());
    }

    @Override
    public void onPause() {
        super.onPause();
        CrimeLab.get(getActivity()).saveCrimes();
    }
}

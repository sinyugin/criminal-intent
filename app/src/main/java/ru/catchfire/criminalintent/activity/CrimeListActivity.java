package ru.catchfire.criminalintent.activity;

import android.support.v4.app.Fragment;

import ru.catchfire.criminalintent.fragment.CrimeListFragment;

public class CrimeListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
    return new CrimeListFragment();
    }

}

package ru.catchfire.criminalintent.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import ru.catchfire.criminalintent.R;

public abstract class SingleFragmentActivity extends FragmentActivity {

    protected abstract Fragment createFragment();
    private FragmentManager mFragMan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        mFragMan = getSupportFragmentManager();
        Fragment fragment = mFragMan.findFragmentById(R.id.fragmentContainer);

        if (fragment == null){
            fragment = createFragment();
            mFragMan.beginTransaction()
                    .add(R.id.fragmentContainer, fragment)
                    .commit();
        }

    }

    public void showToast(String message){
        Toast.makeText(getApplication(), message, Toast.LENGTH_SHORT).show();
    }
}

package ru.catchfire.criminalintent.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.UUID;

import ru.catchfire.criminalintent.R;
import ru.catchfire.criminalintent.fragment.CrimeFragment;
import ru.catchfire.criminalintent.model.Crime;
import ru.catchfire.criminalintent.model.CrimeLab;
import ru.catchfire.criminalintent.utils.ConstantManager;

public class CrimePagerActivity extends FragmentActivity {

    private ArrayList<Crime> mCrimes;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCrimes = CrimeLab.get(getApplicationContext()).getCrimes();
        FragmentManager manager = getSupportFragmentManager();

        mViewPager = new ViewPager(this);
        mViewPager.setId(R.id.view_pager);
        setContentView(mViewPager);

        UUID uuid = (UUID) getIntent().getSerializableExtra(ConstantManager.CRIME_UUID);


        mViewPager.setAdapter(new FragmentStatePagerAdapter(manager) {
            @Override
            public Fragment getItem(int position) {
                return CrimeFragment.newInstance(mCrimes.get(position).getUUID());
            }

            @Override
            public int getCount() {
                return mCrimes.size();
            }
        });

        //устанавливает в качестве заголовка активности заголовот выбранного Crime
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                setTitle(mCrimes.get(position).getTitle());
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });

        //устанавливает в ViewPager выбранный Crime
        for (int i = 0; i < mCrimes.size(); i++){
            if (mCrimes.get(i).getUUID().equals(uuid)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }
}

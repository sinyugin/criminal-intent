package ru.catchfire.criminalintent.model;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import ru.catchfire.criminalintent.utils.JsonSerializer;

/**
 * Хранилище объектов Crime
 */

public class CrimeLab {

    private ArrayList<Crime> mCrimes;
    private static CrimeLab sCrimeLab;
    private Context mContext;

    private static final String TAG = "CrimeLab";
    private static final String FILENAME = "crimes.json";
    private JsonSerializer mJsonSerializer;

    private CrimeLab(Context context) {
        mContext = context;
        mJsonSerializer = new JsonSerializer(mContext, FILENAME);
        try {
            mCrimes = mJsonSerializer.loadCrimes();
        } catch (Exception e) {
            mCrimes = new ArrayList<>();
            for (int i = 0; i < 21; i++){
                Crime crime = new Crime();
                crime.setTitle("Crime # " + i);
                crime.setSolved(i % 2 == 0);
                if (i % 3 ==0){
                    crime.setSolved(false);
                }
                mCrimes.add(crime);
            }
        }
    }

    public static CrimeLab get(Context context){
        if (sCrimeLab == null){
            sCrimeLab = new CrimeLab(context.getApplicationContext());
        }
        return sCrimeLab;
    }

    public ArrayList<Crime> getCrimes(){
        return mCrimes;
    }

    public Crime getCrime(UUID uuid){
        for (Crime crime : mCrimes){
            if (crime.getUUID().equals(uuid))
                return crime;
        }
        return null;
    }

    public boolean saveCrimes(){
        try {
            mJsonSerializer.saveCrimes(mCrimes);
            Log.d(TAG, "crimes have saved to file");
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}

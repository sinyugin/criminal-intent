package ru.catchfire.criminalintent.utils;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

import ru.catchfire.criminalintent.model.Crime;

/**
 * Преобразует crime in JSON object
 */

public class JsonSerializer {

    private String mFilename;
    private Context mContext;

    public JsonSerializer(Context context, String filename) {
        mFilename = filename;
        mContext = context;
    }

    public void saveCrimes(ArrayList<Crime> crimes) throws JSONException, IOException {

        JSONArray array = new JSONArray();

        for (Crime c: crimes) {
            array.put(c.toJSON());
        }

        Writer writer = null;

        try {
            OutputStream out = mContext.openFileOutput(mFilename, Context.MODE_PRIVATE);
            writer = new OutputStreamWriter(out);
            writer.write(array.toString());
        } finally {
            if (writer != null)
                writer.close();
        }
    }

    public  ArrayList<Crime> loadCrimes() throws IOException, JSONException{
        ArrayList<Crime> crimes = new ArrayList<>();
        BufferedReader reader = null;
        try {
            InputStream inputStream = mContext.openFileInput(mFilename);
            reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null){
                builder.append(line);
            }
            JSONArray array = (JSONArray) new JSONTokener(builder.toString()).nextValue();
            for (int i = 0; i < array.length(); i++){
                crimes.add(new Crime((array.getJSONObject(i))));
            }
        } catch (FileNotFoundException e){

        } finally {
            if (reader != null)
                reader.close();
        }
        return crimes;
    }
}

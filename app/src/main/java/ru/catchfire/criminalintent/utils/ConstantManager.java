package ru.catchfire.criminalintent.utils;

/**
 * Содержит константы
 */

public interface ConstantManager {

    // Используем при передаче UUID выбранного объекта Crime
    String CRIME_UUID = "CRIME_UUID";

    // Константа для вызова диалог фрагмента
    // Ключ для передачи даты из CrimeFragment в DatePickerFragment
    String DATE_PICKER_FRAGMENT = "DATE_PICKER_FRAGMENT";

    // Необходим для возвращения Date из DatePickerFragment в CrimeFragment
    int REQUEST_DATE = 0;
}
